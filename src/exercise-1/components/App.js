import React, {Component} from 'react';
import '../styles/App.css';
import {BrowserRouter as Router, Route, NavLink } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <div className="app">
        <Router>
          <div >
            <header className="homeTop">
              <ul className="homeList">
                <li>
                  <NavLink activeClassName="active" to="/about-us">About Us</NavLink>
                </li>
                <li>
                  <NavLink activeClassName="active" to="/my-profile">My Profile</NavLink>
                </li>
                <li>
                  <NavLink  activeClassName="active" exact to="/">Home </NavLink>
                </li>
              </ul>
            </header>
            <main className="homeMain">
              <Route exact path="/" component={Home} />
              <Route path="/my-profile" component={Profile} />
              <Route path="/about-us" component={Us} />
            </main>
          </div>
        </Router>
      </div>
    );
  }
}
function Home() {
  return (
      <div>
        <h2>This is a beautiful page, <br/>And the url is '/'</h2>
      </div>
  );
}

function Profile() {
  return (
      <div>
        <h2>UserName: XXX <br/>Gender: Female <br/> Work: IT Industry <br/>Website: '/my-profile'</h2>
      </div>
  );
}
function Us() {
  return (
      <div>
        <h2>Company: Thoughtworks Local <br/>Location: Xi'an <br/><br/>For more information, <br/>please view our website <a
            href="https://www.thoughtworks.com/" className="link">website </a></h2>
      </div>

  );
}

// export default App;
