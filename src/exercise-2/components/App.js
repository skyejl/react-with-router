import React, {Component} from 'react';
import '../styles/App.css';
import {BrowserRouter as Router, Route, NavLink, Link, Redirect} from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <div className="app">
        <Router>
          <div >
            <header className="homeTop">
              <ul className="homeList">
                <li>
                  <NavLink activeClassName="active" to="/about-us">About Us</NavLink>
                </li>
                <li>
                  <NavLink activeClassName="active" to="/my-profile">My Profile</NavLink>
                </li>
                <li>
                  <NavLink  activeClassName="active" exact to="/Products">Products </NavLink>
                </li>
                <li>
                  <NavLink  activeClassName="active" exact to="/">Home </NavLink>
                </li>
              </ul>
            </header>
            <main className="homeMain">
                <Route exact path="/" component={Home}/>
                <Route exact path="/Products" component={Products}/>
                <Route exact path="/Products/1" component={Bicycle}/>
                <Redirect from='/goods' to='/Products' />
                <Redirect from='*' to='/' />
                <Route exact path="/Products/2" component={Tv}/>
                <Route exact path="/Products/3" component={Pencil}/>
                <Route exact path="/my-profile" component={Profile}/>
                <Route exact path="/about-us" component={Us}/>
            </main>
          </div>
        </Router>
      </div>
    );
  }
}

function Bicycle() {
    return <p className="product-list"><span className="product-title">Product Detail: </span><br/>
        Name: Bicycle<br/>
        Id: 1,<br/>
        Price: 30<br/>
        Quantity: 15<br/>
        Desc: Bicycle is Good<br/>
        URL: /Products/1<br/>
    </p>;
}

function Tv() {
    return <p className="product-list"><span className="product-title">Product Detail: </span><br/>
        Name: Tv<br/>
        Id: 2,<br/>
        Price: 30<br/>
        Quantity: 15<br/>
        Desc: Tv is Good<br/>
        URL: /Products/2<br/>
    </p>;
}
function Pencil() {
    return <p className="product-list"><span className="product-title">Product Detail: </span><br/>
        Name: Pencil<br/>
        Id: 3,<br/>
        Price: 30<br/>
        Quantity: 15<br/>
        Desc: Pencil is Good<br/>
        URL: /Products/3<br/>
    </p>;
}
function Home() {
  return (
      <div>
        <h2>This is a beautiful page, <br/>And the url is '/'</h2>
      </div>
  );
}

function Profile() {
  return (
      <div>
        <h2>UserName: XXX <br/>Gender: Female <br/> Work: IT Industry <br/>Website: '/my-profile'</h2>
      </div>
  );
}
function Us() {
  return (
      <div>
        <h2>Company: Thoughtworks Local <br/>Location: Xi'an <br/><br/>For more information, <br/>please view our  <a
            href="https://www.thoughtworks.com/" className="link">website </a></h2>
      </div>

  );
}
function Products({match}) {
    return (
        <div>
            <h2>All Products:</h2>
            <ul>
                <li>
                    <Link to={`${match.url}/1`}><h3>Bicycle</h3></Link>
                </li>
                <li>
                    <Link to={`${match.url}/2`}><h3>Tv</h3></Link>
                </li>
                <li>
                    <Link to={`${match.url}/3`}><h3>Pencil</h3></Link>
                </li>
            </ul>

        </div>
    );
}


export default App;
